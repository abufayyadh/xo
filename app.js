function xo(str) {
  let jumlahX = 0; //untuk menampung jumlah huruf X
  let jumlahO = 0; //untuk menampung jumlah huruf O

  for (let i = 0; i < str.length; i++) {
    if (str[i] == "x") {
      jumlahX = jumlahX + 1;
    } else {
      jumlahO = jumlahO + 1;
    }
  }

  if (jumlahX == jumlahO) {
    return true;
  } else {
    return false;
  }

  // you can only write your code here!
}

console.log(xo("xoxoxo")); // true
console.log(xo("oxooxo")); // false
console.log(xo("oxo")); // false
console.log(xo("xxxooo")); // true
console.log(xo("xoxooxxo")); // true
